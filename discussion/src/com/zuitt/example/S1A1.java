package com.zuitt.example;

import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        String fName = userInput.nextLine();

        System.out.println("Last Name:");
        String lName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        double num1 = userInput.nextDouble();

        System.out.println("Second Subject Grade:");
        double num2 = userInput.nextDouble();

        System.out.println("Third Subject Grade:");
        double num3 = userInput.nextDouble();

        double ave = Math.floor((num1 + num2 + num3) / 3);
        System.out.println("Good day, " + fName + " " + lName);
        System.out.println("Your grade average is: " + ave);
    }
}
